package com.khpi.menu_project;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.khpi.bookmanager.Book;

import com.nbsp.materialfilepicker.MaterialFilePicker;



import java.io.FileInputStream;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;


public class MainMenuFragment extends Fragment {
    private View view;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main_menu, container, false);

        callFileChooser();

        loadAllBooks();

        return view;
    }


    private void callFileChooser() {
        if (ContextCompat.checkSelfPermission(
                Objects.requireNonNull(getContext()), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                    Objects.requireNonNull(getActivity()), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
        }


        view.findViewById(R.id.open_new_book).setOnClickListener(e -> {
            new MaterialFilePicker()
                    .withActivity(getActivity())
                    .withRequestCode(1000)
                    .withFilter(Pattern.compile(".*\\.txt$"))
                    .withFilterDirectories(false)
                    .withHiddenFiles(false)
                    .start();

        });

    }

    private List<Book> getListBooks() {
        List<Book> l = new ArrayList<>();
        for (String name : Objects.requireNonNull(getActivity()).fileList()) {

            if (name.equals("current")) {
                continue;
            }
            try (FileInputStream fileOutputStream = getActivity().openFileInput(name);
                 ObjectInputStream objectInputStream = new ObjectInputStream(fileOutputStream)) {
                l.add((Book) objectInputStream.readObject());
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        return l;
    }

    void loadAllBooks() {
        LinearLayout layout = view.findViewById(R.id.lincontainer);
        List<Book> bs = getListBooks();

        layout.removeViews(1, layout.getChildCount() - 1);

        for (Book b : bs) {
            LinearLayout row = new LinearLayout(this.getContext());
            row.setLayoutParams(new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));


            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);

            TextView name = new TextView(this.getContext());
            name.setText(b.getName() + "\t\t");

            TextView count = new TextView(this.getContext());
            count.setText(b.getPercent() + "\t\t");

            Button remove = new Button(this.getContext());
            remove.setText(R.string.remove);
            remove.setOnClickListener(e -> {
                getActivity().deleteFile(b.getName());
                loadAllBooks();
            });

            row.setOnClickListener(v -> {
                FragmentTransaction t = this.getFragmentManager().beginTransaction();
                try {
                    b.serialize(getActivity().openFileOutput("current", Context.MODE_PRIVATE));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                ReadFragment mFrag = new ReadFragment();
                t.replace(R.id.fragment_container, mFrag);
                t.commit();
            });
            name.setLayoutParams(p);
            count.setLayoutParams(p);
            remove.setLayoutParams(p);

            row.addView(name);
            row.addView(count);
            row.addView(remove);

            layout.addView(row);
        }
    }
}
