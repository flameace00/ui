package com.khpi.menu_project;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.widget.ImageButton;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;


import com.khpi.bookmanager.Book;
import com.khpi.bookmanager.FuncReaderImplementation;
import com.khpi.bookmanager.FunctionalReader;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Objects;

public class ReadFragment extends Fragment {
    private View view;
    private FunctionalReader reader;
    private Thread thread = null;
    private TextView[] textView = new TextView[7];
    private TextView mainView;


    private void init(String[] cur) {
        mainView.setText(cur[3]);
        for (int i = 0; i < 7; i++) {
            textView[i].setText(cur[i]);
        }
    }

    private void save(){
        try {
            reader.save(Objects.requireNonNull(getActivity()).openFileOutput(reader.getName(), Context.MODE_PRIVATE));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_read, container, false);

        try (FileInputStream fileOutputStream = getActivity().openFileInput("current");
             ObjectInputStream objectInputStream = new ObjectInputStream(fileOutputStream)) {
            reader = new FuncReaderImplementation((Book) objectInputStream.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        mainView = view.findViewById(R.id.mainText);
        textView[0] = view.findViewById(R.id.text0);
        textView[1] = view.findViewById(R.id.text1);
        textView[2] = view.findViewById(R.id.text2);
        textView[3] = view.findViewById(R.id.text3);
        textView[4] = view.findViewById(R.id.text4);
        textView[5] = view.findViewById(R.id.text5);
        textView[6] = view.findViewById(R.id.text6);

        init(reader.read());
        ImageButton play = view.findViewById(R.id.play);
        ImageButton stop = view.findViewById(R.id.stop);
        ImageButton next = view.findViewById(R.id.nextt);
        ImageButton prev = view.findViewById(R.id.prev);
        ImageButton prevSent = view.findViewById(R.id.prevSent);
        ImageButton nextSent = view.findViewById(R.id.nextSent);


        nextSent.setOnClickListener(e -> {
            if (thread == null) {
                init(reader.nextSentence());
                save();
            }
        });
        prev.setOnClickListener(e -> {
            if (thread == null) {
                init(reader.prevWord());
                save();
            }
        });
        prevSent.setOnClickListener(e -> {
            if (thread == null) {
                init(reader.prevSentence());
                save();
            }
        });
        next.setOnClickListener(e -> {
            if (thread == null) {
                init(reader.nextWord());
                save();
            }
        });


        Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);

        play.setOnClickListener(e -> {
            if (thread != null) {
                return;
            }
            ((DrawerLocker) (getActivity())).setDrawerEnabled(false);

            thread = new Thread(() -> {
                boolean isReading = true;
                int current = reader.getReader().getCursor();
                int max = reader.getReader().getWordsCount() - 1;
                while (isReading) {
                    init(reader.nextWord());
                    try {
                        int sleepTime = Integer.parseInt(((TextView) view.findViewById(R.id.num1)).getText().toString());
                        Thread.sleep(Math.max(sleepTime, 30));
                    } catch (InterruptedException ex) {
                        break;
                    }
                    isReading = ++current != max;
                }
                thread = null;
            });
            thread.start();
        });
        stop.setOnClickListener(e -> {
            if (thread == null) {
                return;
            }
            ((DrawerLocker) (getActivity())).setDrawerEnabled(true);
            thread.interrupt();
            thread = null;
            save();
        });
        hideSystemUI();
        Objects.requireNonNull(getActivity()).getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        return view;
    }

    private void hideSystemUI() {
        View decorView = view;
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY

                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }
    private void showSystemUI() {
        View decorView = view;
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public void setReader(FunctionalReader reader) {
        this.reader = reader;
    }
}