package com.khpi.menu_project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MenuItem;

import android.view.View;

import android.widget.EditText;

import com.google.android.material.navigation.NavigationView;
import com.khpi.bookmanager.Book;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashSet;



public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DrawerLocker {
    private DrawerLayout drawer;


    @Override
    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        drawer.setDrawerLockMode(lockMode);
    }

    public void onButtonClick(View v) {
        EditText numb = findViewById(R.id.num1);
        findViewById(R.id.num1);
        int num1 = Integer.parseInt(numb.getText().toString());
        num1 += 10;
        numb.setText(Integer.toString(num1));
    }

    public void onButtonClick1(View b) {
        EditText numb = findViewById(R.id.num1);
        int num1 = Integer.parseInt(numb.getText().toString());
        num1 -= 10;
        if (num1 < 0) {
            num1 = 0;
        }
        numb.setText(Integer.toString(num1));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new MainMenuFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_main_menu);


        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        switch (item.getItemId()) {

            case R.id.nav_main_menu: {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new MainMenuFragment()).commit();
                break;
            }
            case R.id.nav_help:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new HelpFragment()).commit();
                break;
            case R.id.nav_exit:
                System.exit(0);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000 && resultCode == RESULT_OK) {
            String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            assert filePath != null;
            Book b = new Book(filePath);
            if (new HashSet<>(Arrays.asList(fileList())).contains(b.getName())) {
                return;
            }
            try {
                b.serialize(openFileOutput(b.getName(), MODE_PRIVATE));
                ((MainMenuFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container)).loadAllBooks();
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
