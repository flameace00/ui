package com.khpi.menu_project;


interface DrawerLocker {
    void setDrawerEnabled(boolean enabled);

}
