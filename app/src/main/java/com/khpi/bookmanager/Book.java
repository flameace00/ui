package com.khpi.bookmanager;

import android.annotation.SuppressLint;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;

/**
 * path - path to txt file in folder
 * cursor - current word in this book
 */

final public class Book implements Serializable {

    final private String path;
    final private String name;
    private int countWords = 0;
    private int cursor = 0;

    public Book(final String path) {
        this.path = path;
        this.name = path.substring(path.lastIndexOf('/') + 1);
    }

    public void serialize(OutputStream outputStream) {

        try (ObjectOutputStream os = new ObjectOutputStream(outputStream)) {
            os.writeObject(this);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPath() {
        return path;
    }

    public int getCursor() {
        return cursor;
    }

    public void setCursor(final int newCursor) {
        if (newCursor < 0) {
            throw new RuntimeException("Cursor can't be less than 0");
        }
        this.cursor = newCursor;
    }

    int getCountWords() {
        return countWords;
    }

    void setCountWords(int countWords) {
        this.countWords = countWords;
    }

    int incrementCursor() {
        return ++this.cursor;
    }

    int decrementCursor() {
        if (this.cursor != 0) {
            this.cursor--;
        }
        return this.cursor;
    }

    public String getName() {
        return name;
    }

    @SuppressLint("DefaultLocale")
    public String getPercent() {
        if (countWords == 0) {
            return "0";
        }

        return String.format("%.2f%%", (double) (cursor) / countWords);
    }
}
