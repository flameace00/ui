package com.khpi.bookmanager;

import java.io.OutputStream;

public interface FunctionalReader {
    String[] read();

    String[] nextWord();

    String[] prevWord();

    String[] nextSentence();

    String[] prevSentence();

    BookReader getReader();

    void save(OutputStream os);

    String getName();
}
