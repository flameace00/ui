package com.khpi.bookmanager;

import java.io.OutputStream;

import static java.lang.System.arraycopy;

public class FuncReaderImplementation implements FunctionalReader {

    private final BookReader bookReader;
    private final String[] words = new String[7];

    public FuncReaderImplementation(Book book) {
        this.bookReader = new BookReader(book);
        initArray();
    }

    private void initArray() {
        int cur = bookReader.getCursor();
        for (int i = cur - 3, j = 0; i <= cur + 3; i++, j++) {
            try {
                words[j] = bookReader.getByIndex(i);
            } catch (Exception ignored) {
                words[j] = "";
            }
        }
    }

    @Override
    public String[] read() {
        return words;
    }

    @Override
    public String[] nextWord() {
        if (bookReader.getCursor() == bookReader.getWordsCount() - 1) {
            return words;
        }
        bookReader.readNext();
        arraycopy(words, 1, words, 0, 6);
        try {
            words[6] = bookReader.getByIndex(bookReader.getCursor() + 3);
        } catch (Exception ignored) {
            words[6] = "";
        }
        return words;
    }

    @Override
    public String[] prevWord() {
        if (bookReader.getCursor() == 0) {
            return words;
        }
        bookReader.readPrev();
        System.arraycopy(words, 0, words, 1, 6);
        try {
            words[0] = bookReader.getByIndex(bookReader.getCursor() - 3);
        } catch (Exception ignored) {
            words[0] = "";
        }
        return words;
    }

    @Override
    public String[] nextSentence() {
        bookReader.nextSentence();
        initArray();
        return words;
    }

    @Override
    public String[] prevSentence() {
        bookReader.prevSentence();
        initArray();
        return words;
    }

    @Override
    public BookReader getReader() {
        return bookReader;
    }

    @Override
    public void save(OutputStream os) {
        bookReader.save(os);
    }

    @Override
    public String getName() {
        return bookReader.getName();
    }
}