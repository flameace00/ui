package com.khpi.bookmanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

    public final class BookReader {
        private final Book book;
        private final List<String> words = new ArrayList<>();

        BookReader(final Book book) {
            this.book = book;
            loadAllWords(book.getPath());
            book.setCountWords(words.size());
        }

        public int getWordsCount() {
            return book.getCountWords();
        }

        private void loadAllWords(final String path) {
            for (String s : readAllLines(path)) {
                words.addAll(Arrays.asList(s.split(" ")));
            }
        }

        private List<String> readAllLines(final String path) {
            try (BufferedReader reader = new BufferedReader(new FileReader(new File(path)))) {
                List<String> result = new ArrayList<>();
                for (; ; ) {
                    String line = reader.readLine();
                    if (line == null)
                        break;
                    result.add(line);
                }
                return result;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }

        /**
         * Get next word.
         *
         * @return next word from cursor.
         */

        String readNext() {
            return words.get(book.incrementCursor());
        }

        /**
         * Previous word of cursor
         *
         * @return Previous word
         */
        String readPrev() {
            return words.get(book.decrementCursor());
        }

        /**
         * Get current word
         *
         * @return current word of book cursor
         */

        public String read() {
            return words.get(book.getCursor());
        }

        /**
         * Go to start of the next sentence.
         */

        void nextSentence() {
            while (book.getCursor() + 1 < words.size() && !readNext().contains(".")) ;
            if (getCursor() != words.size() - 1) {
                book.incrementCursor();
            }
        }

        /**
         * Back to start of current sentence.
         * Or start of previous sentence if it at start current sentence
         */

        void prevSentence() {
            book.decrementCursor();
            while (!readPrev().contains(".") && book.getCursor() != 0) ;
            if (book.getCursor() != 0) {
                book.incrementCursor();
            }
        }

        public int getCursor() {
            return book.getCursor();
        }

        String getByIndex(int i) {
            return words.get(i);
        }

        void save(OutputStream os) {
            book.serialize(os);
        }

        String getName() {
            return book.getName();
        }
    }
